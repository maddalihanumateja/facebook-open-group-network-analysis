Facebook open group network analysis
====================================

Usage
-----

This requires PHP 5.4 or greater and the latest stable version of R. 

For OS X users:
 
- Add your own facebook app id and app secret (Not the same as facebook login id and password) in the php files (fb_group_members.php and fb_group_feed.php) only where specified by a comment. 
![Screen Shot 2014-11-22 at 2.50.33 PM.png](https://bitbucket.org/repo/no5dMp/images/3636252633-Screen%20Shot%202014-11-22%20at%202.50.33%20PM.png)                  
If havent created a facebook app previously go to https://developers.facebook.com/ and create a new app for websites. You should get a screen similar to the image below after app creation.
![Screen Shot 2014-11-22 at 2.44.19 PM.png](https://bitbucket.org/repo/no5dMp/images/486336503-Screen%20Shot%202014-11-22%20at%202.44.19%20PM.png)

- Enter the group id of the public FB group of your interest. for example: "Cystic Fibrosis Group" https://www.facebook.com/groups/2207539081/  has group id: 2207539081. There are some groups where the group id isn't visible for example "https://www.facebook.com/groups/ihavetypeonediabetes/". I havent tried just inputting "ihavetypeonediabetes" as the group id but there are websites that can find it out. (http://lookup-id.com/)

- You can also change the number of pages of data that will be requested from the API.

- Double click the "get_Fb_Group_Nwk.command" file. You will need to have R (check by typing "R" on terminal) and PHP (check by typing "php -v" on terminal) installed on your machine. If the command file gives an error, open a terminal window and change current directory to the folder with the scripts. then type the following
php fb_group_feed.php > fb_messages.json 
php fb_group_members.php > fb_actors.json
Rscript getFbGroupNetworkFromJson.R

- The output will be 2 json files (fb_messages.json and fb_actors.json) and 2 csv files (actors.csv and edges.csv) in the same folder.

If theres a message saying something like "you dont have sufficient authorization to run this file" after double clicking the "get_Fb_Group_Nwk.command" file
then run the following 
sudo chmod 700 get_Fb_Group_Nwk.command 
in terminal. and try double clicking again.
I havent tried using this on windows. But it should be simple enough to create an exe file if PHP and R are installed