EXEC_DIR=$(dirname ${BASH_SOURCE[0]})
CURRENT_DIR=$(pwd)

cd $EXEC_DIR
php fb_group_feed.php > fb_messages.json 
php fb_group_members.php > fb_actors.json
Rscript getFbGroupNetworkFromJson.R
cd $CURRENT_DIR