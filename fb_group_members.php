<?php
require_once("autoload.php");
use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\GraphUser;
use Facebook\FacebookRequestException;
use Facebook\FacebookRedirectLoginHelper;

$max_num_pages=12;//Max number of search result pages to fetch. (Facebook performs pagination of the results.)
$group_id='2207539081';//Only public group info can be requested. //T1D group ID is 2224073208//Cystic Fibrosis Facebook Group ID is 2207539081

//Input your app id and app secret inside the respective quotes after creating a facebook developer account
$app_id='';
$app_secret='';
$end_point='https:\/\/graph\.facebook\.com\/v[0-9]\.[0-9]';
FacebookSession::setDefaultApplication($app_id,$app_secret);

// Use one of the helper classes to get a FacebookSession object.
//   FacebookRedirectLoginHelper
//   FacebookCanvasLoginHelper
//   FacebookJavaScriptLoginHelper
// or create a FacebookSession with a valid access token:
$access_token = $app_id.'|'.$app_secret;
//echo $access_token.'\n';
$session = new FacebookSession($access_token);
// Get the GraphUser object for the current user:

try {
  //echo "Trying it out\n";
  $query='/'.$group_id.'/?fields=members.limit(1000000),owner,name,updated_time,description,id&date_format=m/d/Y h:i:s A';
  $me = (new FacebookRequest($session, 'GET',$query))->execute()->getGraphObject()->asArray();
  
  
  $members=$me[members];
  $next=$members->paging->next;
  $next_exists=(int)(!empty($next));

  $page_counter=1;


  //Looping over facebook pagination
  while($next_exists && $page_counter<$max_num_pages) {
  	$next=preg_replace('/'.$end_point.'/', '', $next, 1);
  	$me2 = (new FacebookRequest($session, 'GET',$next))->execute()->getGraphObject()->asArray();
  	$members2=$me2[data];
  	if(!empty($members2)){
  		$me[members]->data=array_merge($me[members]->data,$members2);
  	}
  	$next=$me2[paging]->next;
  	$next_exists=(int)(!empty($next));
  	$page_counter++;
  }
  $json_me=json_encode($me);
  print_r($json_me);
} catch (FacebookRequestException $e) {
  echo "The Graph API returned an error\n";
} catch (\Exception $e) {
  echo "Some other error occurred\n";
}
?>